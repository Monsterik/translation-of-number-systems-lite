#include "tst_autotest.h"

void AutoTest::test_f10to2()
{
    QString in = "432";
    QString out = "110110000";
    QCOMPARE(out, f10to2(in));

    in = "534555";
    out = "10000010100000011011";
    QCOMPARE(out, f10to2(in));

    in = "AAAA";
    out = "";
    QCOMPARE(out, f10to2(in));
}

void AutoTest::test_f16to8()
{
    QString in = "ABABF";
    QString out = "2535277";
    QCOMPARE(out, f16to8(in));

    in = "AGD";
    out = "";
    QCOMPARE(out, f16to8(in));

    in = "0";
    out = "0";
    QCOMPARE(out, f16to8(in));
}

void AutoTest::test_f8to2()
{
    QString in = "765";
    QString out = "111110101";
    QCOMPARE(out, f8to2(in));

    in = "64568434";
    out = "";
    QCOMPARE(out, f8to2(in));

    in = "dsaf";
    out = "";
    QCOMPARE(out, f8to2(in));
}

void AutoTest::test_f8to16()
{
    QString in = "3321";
    QString out = "6D1";
    QCOMPARE(out, f8to16(in));

    in = "945654";
    out = "";
    QCOMPARE(out, f8to16(in));

    in = "5 5";
    out = "";
    QCOMPARE(out, f8to16(in));
}

void AutoTest::test_f2to10()
{
    QString in = "1010011010";
    QString out = "666";
    QCOMPARE(out, f2to10(in));

    in = "432/fasdfasdf234";
    out = "";
    QCOMPARE(out, f2to10(in));

    in = "56";
    out = "";
    QCOMPARE(out, f2to10(in));
}

void AutoTest::test_f2to8()
{
    QString in = "1011010";
    QString out = "132";
    QCOMPARE(out, f2to8(in));

    in = "01012101";
    out = "";
    QCOMPARE(out, f2to8(in));

    in = ";;;;;;;;;;;";
    out = "";
    QCOMPARE(out, f2to8(in));
}
