#ifndef TST_AUTOTEST_H
#define TST_AUTOTEST_H

#include <QtTest>
#include "../functions.h"

class AutoTest : public QObject
{
    Q_OBJECT

private slots:
    void test_f10to2();
    void test_f16to8();
    void test_f8to2();
    void test_f8to16();
    void test_f2to8();
    void test_f2to10();
};

#endif // TST_AUTOTEST_H
