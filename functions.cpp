#include "functions.h"

QString toHex(QList<int> numbers)
{
    QString out = "";
    for(int i=0 ;i < numbers.length();i++)
    {
        if (numbers[i] < 10)
        {
            out.append(QString::number(numbers[i]));
        }
        else if(numbers[i]==10)
        {
            out.append("A");
        }
        else if(numbers[i]==11)
        {
            out.append("B");
        }
        else if(numbers[i]==12)
        {
            out.append("C");
        }
        else if(numbers[i]==13)
        {
            out.append("D");
        }
        else if(numbers[i]==14)
        {
            out.append("E");
        }
        else if(numbers[i]==15)
        {
            out.append("F");
        }
    }
    return out;
}

QString f16to10(QString input)
{
    if(!checkValid(input, 16)) {return "";}
    QList<int> numbers;
    for(int i=0 ;i < input.size();i++)
    {
        if (input[i].digitValue() < 10 && input[i].isDigit())
        {
            numbers.append(input[i].digitValue());
        }
        else if(input[i] == "A")
        {
            numbers.append(10);
        }
        else if(input[i] == "B")
        {
            numbers.append(11);
        }
        else if(input[i] == "C")
        {
            numbers.append(12);
        }
        else if(input[i] == "D")
        {
            numbers.append(13);
        }
        else if(input[i] == "E")
        {
            numbers.append(14);
        }
        else if(input[i] == "F")
        {
            numbers.append(15);
        }
    }

    int value = 0;
    int k = 1;

    for(int i = numbers.length()-1; i >= 0 ;i--)
    {
        value += numbers[i] * k;
        k = k * 16;
    }
    return QString::number(value);
}

QString f16to8(QString input)
{
    return f10to8(f16to10(input));
}

QString f16to2(QString input)
{
    return f10to2(f16to10(input));
}

QString f10to2(QString input)
{
    if(!checkValid(input, 10)) {return "";}
    int number = input.toInt();
    QString output = "";
    do
    {
        output.push_front(QString::number(number % 2));
        number /= 2;
    } while (number);
    return  output;
}

QString f10to8(QString input)
{
    if(!checkValid(input, 10)) {return "";}
    int number = input.toInt();
    QString output = "";
    do
    {
        output.push_front(QString::number(number % 8));
        number /= 8;
    } while(number);
    return output;
}

QString f10to16(QString input)
{
    if(!checkValid(input, 10)) {return "";}
    int number = input.toInt();
    int output = 0;
    QList<int> numbers;
    for(int i=0; number!=0 ;i++)
    {
        output = number % 16;
        numbers.push_front(output);
        number /= 16;
    }
    return toHex(numbers);

}

QString f8to10(QString input)
{
    if(!checkValid(input, 8)) {return "";}
    int number = input.toInt();
    int output = 0;
    int rank = 1;
    do
    {
        output += number % 10 * rank;
        number /= 10;
        rank *= 8;
    } while (number);
    return QString::number(output);
}

QString f8to16(QString input)
{
    return f10to16(f8to10(input));
}

QString f8to2(QString input)
{
    return f10to2(f8to10(input));
}

QString f2to10(QString input)
{
    if(!checkValid(input, 2)) {return "";}
    int number = input.toInt();
    int output = 0;
    int rank = 1;
    do
    {
        output += number % 10 * rank;
        number /= 10;
        rank *= 2;
    } while (number);
    return  QString::number(output);
}

QString f2to16(QString input)
{
    return f10to16(f2to10(input));
}

QString f2to8(QString input)
{
    return f10to8(f2to10(input));
}

bool checkValid(QString input, int system)
{
    if (input == "") {return 0;}
    for (int i =0;i < input.size();i++)
    {
        if (input[i].isDigit())
        {
            if (input[i].digitValue() >= system)
            {
                return 0;
            }
        }
        else
        {
            if (system == 16)
            {
                if(!(input[i] == "A" || input[i] == "B" || input[i] == "C" || input[i] == "D" || input[i] == "E" || input[i] == "F"))
                {
                    return 0;
                }
            }
            else return  0;
        }
    }
    return 1;
}
