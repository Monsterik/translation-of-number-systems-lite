#ifndef FUNCTIONS_H
#define FUNCTIONS_H

#include <QString>
#include <QList>

QString toHex(QList<int> numbers);

QString f16to10(QString input);
QString f16to8(QString input);
QString f16to2(QString input);

QString f8to10(QString input);
QString f8to16(QString input);
QString f8to2(QString input);

QString f10to2(QString input);
QString f10to8(QString input);
QString f10to16(QString input);

QString f2to10(QString input);
QString f2to16(QString input);
QString f2to8(QString input);

bool checkValid(QString input, int system);

#endif // FUNCTIONS_H
