#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "item.h"
#include "functions.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

void MainWindow::filltable()
{
    ui->historyList->clear();
    for(int i = 0; i < items.size(); ++i)
    {
        ui->historyList->addItem(items[i].inputnumber + "(" + QString::number(items[i].inputsystem) + ")"
                                 + "=" + items[i].outputnumber + "(" + QString::number(items[i].outputsystem) + ")");
    }

}

void MainWindow::on_pushButton_clicked()
{
    if (!checkValid(ui->inputLine->text(), ui->inputSystem->currentText().toInt()))
    {
        QMessageBox messageBox;
        messageBox.critical(this,"Ошибка","Ошибка ввода или недопустимые значения");

        return;
    }

    Item item;
    QString output;

    item.inputnumber = ui->inputLine->text();

    int size = item.inputnumber.length();

    for (int i =0 ; i < size ; i++)
    {
        if (i == size-1) {break;}
        else if (item.inputnumber[0] == "0") {item.inputnumber.remove(0,1);}
        else {break;}
    }

    item.inputsystem = ui->inputSystem->currentText().toInt();
    item.outputsystem = ui->outputSystem->currentText().toInt();

    if (item.inputsystem == item.outputsystem)
    {
        output = item.inputnumber;
    }
    else
    {
        if (item.inputsystem == 16 && item.outputsystem == 10){output = f16to10(item.inputnumber);}
        if (item.inputsystem == 16 && item.outputsystem == 8){output = f16to8(item.inputnumber);}
        if (item.inputsystem == 16 && item.outputsystem == 2){output = f16to2(item.inputnumber);}
        if (item.inputsystem == 10 && item.outputsystem == 16){output = f10to16(item.inputnumber);}
        if (item.inputsystem == 10 && item.outputsystem == 8){output = f10to8(item.inputnumber);}
        if (item.inputsystem == 10 && item.outputsystem == 2){output = f10to2(item.inputnumber);}
        if (item.inputsystem == 8 && item.outputsystem == 16){output = f8to16(item.inputnumber);}
        if (item.inputsystem == 8 && item.outputsystem == 10){output = f8to10(item.inputnumber);}
        if (item.inputsystem == 8 && item.outputsystem == 2){output = f8to2(item.inputnumber);}
        if (item.inputsystem == 2 && item.outputsystem == 16){output = f2to16(item.inputnumber);}
        if (item.inputsystem == 2 && item.outputsystem == 10){output = f2to10(item.inputnumber);}
        if (item.inputsystem == 2 && item.outputsystem == 8){output = f2to8(item.inputnumber);}
    }
    item.outputnumber = output;
    items.push_back(item);

    ui->outputLine->setText(output);

    filltable();
}

MainWindow::~MainWindow()
{
    delete ui;
}
