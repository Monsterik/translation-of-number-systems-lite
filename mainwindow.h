#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "item.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    QList<Item> items;
    ~MainWindow();

private slots:
    void on_pushButton_clicked();
    void filltable();
private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
